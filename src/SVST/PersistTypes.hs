{-# LANGUAGE OverloadedStrings #-}
module SVST.PersistTypes where

import           Data.ByteString (ByteString)
import           Data.Hex
import           Database.Persist.Sql


instance PersistField SHA256 where
  toPersistValue (SHA256 bs) = toPersistValue bs
  fromPersistValue v = SHA256 <$> fromPersistValue v

instance PersistFieldSql SHA256 where
  sqlType _ = SqlOther "bytea"

instance PersistField Hex160 where
  toPersistValue (Hex160 bs) = toPersistValue bs
  fromPersistValue v = Hex160 <$> fromPersistValue v

instance PersistFieldSql Hex160 where
    sqlType _ = SqlOther "bytea"

instance PersistField Prefix6 where
  toPersistValue (Prefix6 bs) = toPersistValue bs
  fromPersistValue v = Prefix6 <$> fromPersistValue v

instance PersistFieldSql Prefix6 where
    sqlType _ = SqlOther "bytea"


-- 64 hex chars = 32 bytes
newtype SHA256 = SHA256 ByteString
  deriving (Read, Eq)

-- 160 hex chars = 80 bytes
newtype Hex160 = Hex160 ByteString
  deriving (Read, Eq)

-- 6 bytes chars
newtype Prefix6 = Prefix6 ByteString
  deriving (Read, Eq)

instance Show SHA256 where
  show (SHA256 bs) = "SHA256 (hex " ++ show (hex bs) ++ ")"

instance Show Hex160 where
  show (Hex160 bs) = "Hex160 (hex " ++ show (hex bs) ++ ")"

instance Show Prefix6 where
  show (Prefix6 bs) = "Prefix6 (hex " ++ show (hex bs) ++ ")"

sha256FromHex :: ByteString -> Maybe SHA256
sha256FromHex bs = SHA256 <$> unhex bs

hex160FromHex :: ByteString -> Maybe Hex160
hex160FromHex bs = Hex160 <$> unhex bs

prefix6FromHex :: ByteString -> Maybe Prefix6
prefix6FromHex bs = Prefix6 <$> unhex bs
