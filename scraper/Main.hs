{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Concurrent
import           Control.Concurrent.Async
import           Control.Monad
import           Data.Int
import qualified Data.Text.Lazy.IO as T
import           Data.Time
import           Formatting
import           Options.Applicative
import           System.Exit
import           System.IO
import           System.Posix.Signals

-- friends
import           SVST.NullData
import           SVST.Util


progressDelaySecs :: Int
progressDelaySecs = 1

waitForNewBlockDelaySecs :: Int
waitForNewBlockDelaySecs = 3

blockChunkSize :: BlockIndex
blockChunkSize = 100

-- TODO: Perhaps make this configurable on command line?
numThreads :: Int
numThreads = 4


--
-- So that we don't have to worry about Bitcoin chain reorganisations, we only
-- scraped null data from `blockLag` blocks ago. This makes it extraordinarily
-- unlikely that the Bitcoin block chain will undergo a chain reorg.
-- (https://en.bitcoin.it/wiki/Chain_Reorganization)
--
blockLag :: Int32
blockLag = 3

--------------------------------------------------------------------------------
-- Command line arguments

data Opts =
  Opts {
    optStartBlock :: BlockIndex
  } deriving (Read, Show, Eq)

optParser :: Parser Opts
optParser =
      Opts
  <$> (read <$> strOption (long "start" <> metavar "BLOCKINDEX" <>
                           value "1" <> help "Start at block BLOCKINDEX (minimum 1)"))

optInfo :: ParserInfo Opts
optInfo = info (helper <*> optParser)
               (fullDesc <> progDesc "A program to scrape SVST null data")

--------------------------------------------------------------------------------
--
-- FIXME: Have program take a optional block range on the command line
-- Either that or read database to determine last block looked at.
--
main :: IO ()
main = do
  hSetBuffering stdout LineBuffering
  mainThreadId <- myThreadId
  opts         <- execParser $ optInfo
  heightMVar   <- newMVar $ max (optStartBlock opts) 1  -- min block height is 1
  numBlocks    <- retryForever getBlockHeight
  forkIO (timeThread heightMVar)
  let threads = replicate numThreads
                  (scrapeContinuously heightMVar numBlocks)
  asyncs <- mapM async threads
  let catcher = Catch $ exitThreads mainThreadId
  installHandler sigINT catcher Nothing
  mapM_ wait asyncs
  return ()

exitThreads :: ThreadId -> IO ()
exitThreads tid = throwTo tid ExitSuccess

timeThread :: MVar BlockIndex -> IO ()
timeThread mvar = getCurrentTime >>= go
  where
    go t = do
      threadDelaySecs $ progressDelaySecs
      withMVar mvar $ \height -> do
        t' <- getCurrentTime
        blockHeight <- retryForever getBlockHeight
        let diff = diffUTCTime t' t
        T.putStrLn $ format ("[" % string % "]: " %
                             "Block height: " % int % " " %
                             "Scraped: " % int % "/" % int % " (lag = " %
                               int % " blocks) " %
                             "(elapsed: " % fixed 2 % "s)")
          (isoTime t') blockHeight height (blockHeight - blockLag) blockLag diff
      go t

allConcurrently :: [IO a] -> IO [a]
allConcurrently ios = mapM runConcurrently (map Concurrently ios)

allConcurrently_ :: [IO a] -> IO ()
allConcurrently_ ios = allConcurrently ios >> return ()

scrapeContinuously :: MVar BlockIndex -> BlockIndex -> IO ()
scrapeContinuously mvar blockFinishAtBegin =
  go blockFinishAtBegin
  where
    go finish = do
      let scrapeTo = finish - blockLag
      lh <- readMVar mvar -- atomic read
      if lh >= scrapeTo
        then do
          -- since we started the number of blocks might have increased
          finishNow <- retryForever getBlockHeight
          when (lh == finishNow - blockLag) $ do
            threadDelaySecs $ waitForNewBlockDelaySecs
          go finishNow
        else do
          scrapeNextBlockChunk mvar scrapeTo
          go finish

--
-- heightMVar contains the block height of the _last_ block that was
-- scraped
--
scrapeNextBlockChunk :: MVar BlockIndex -> BlockIndex -> IO ()
scrapeNextBlockChunk mvar numBlocks = do
  lastHeight <- takeMVar mvar
  let newLastHeight = min (lastHeight + blockChunkSize) numBlocks
  -- T.putStrLn $ format ("Scraping nulldata from blocks " % int % " - " %
  --                     int) (lastHeight + 1) newLastHeight
  putMVar mvar $ newLastHeight
  retryForever $ do
    scrapeNullDatas $ BlockRange (lastHeight + 1) newLastHeight
    return $ Right ()

