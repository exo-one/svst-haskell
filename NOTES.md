NOTES


- The svst-nulldata-scraper should just scrape. It should handle Bitcoin
  blockchain re-orgs. We should let bitcoind handle the re-orgs.
  This means that every so often (perhaps every minute) or so we check
  the last 144 blocks to see if they have changed. If so we update our
  own database.

  We should have a table for block headers. We have a local ID for each
  of the block headers and we use that as a foreign key in the null_data
  table.

  Also, on start up we have to verify that all the blocks we have are
  valid we do this by
    - `getblockcount` to get the height
    - repeated calls to `getblockhash` to get hashes
    - if we find a different hash we roll back and rescan from that point.

- There should be another executable that does the verification. It
  monitors the database instead of doing `bitcoin-cli` calls.
  Once we find that there is a new block we immediately get its nulldatas
  and check for nulldatas prefixed with 'svst-test' and then
   - download the pallet header
   - download the pallet referred to in the pallet header after having
     checked the signature


  IMPORTANT: This should have a --no-verify flag that just downloads pallets
  and "pins" them (in the IPFS sense) so that other people can download
  them quickly on the day.

# Data Volume Container

I will have to create a special volume with data for:

- Postgres
- Bitcoin
- IPFS


# How I published an OP_RETURN

QmZidicD89an7mrQMUPY6Pzgkz5PtNAKcJBZ9yYhtTqyRY =
1220a912368aec8529049d1261b95f4c60af5e6bf0a9438d56e59ce4e770720fb6ef

FluxSTTest = 466c7578535454657374


Full message: 466c75785354546573741220a912368aec8529049d1261b95f4c60af5e6bf0a9438d56e59ce4e770720fb6ef

txId: a24755082797571142f65463972e3dc4909bde2fa75a3833b149dcafa4d386b5
vout: 0

**Final command**:

bitcoin-cli createrawtransaction '[{ "txid": "a24755082797571142f65463972e3dc4909bde2fa75a3833b149dcafa4d386b5", "vout": 0}]' '{ "1LFCYXREtjnHsRgSvVR2E3o2geh3Vdc6Ly": 0.00175, "data": "466c75785354546573741220a912368aec8529049d1261b95f4c60af5e6bf0a9438d56e59ce4e770720fb6ef" }'

-------

Second OP_RETURN

QmZreKzTqfCgMF98N3keES4UfmrcC4r6xXv2ywpDDx9mzk =
1220ab1f8ca0f3ea808471c3a8dfd9af5562c2c191e53130e41409a879b2ad5e7cf5

TestFluxST = 54657374466c75785354

Full message: 54657374466c757853541220ab1f8ca0f3ea808471c3a8dfd9af5562c2c191e53130e41409a879b2ad5e7cf5

bitcoin-cli createrawtransaction '[{ "txid": "4f298a2df9ab5d74a4495999f4db43f95533ab0807244579202cbb6c096dfc67", "vout": 0}]' '{ "1LFCYXREtjnHsRgSvVR2E3o2geh3Vdc6Ly": 0.00150, "data": "54657374466c757853541220ab1f8ca0f3ea808471c3a8dfd9af5562c2c191e53130e41409a879b2ad5e7cf5" }'

Gives output:

010000000167fc6d096cbb2c207945240708ab3355f943dbf4995949a4745dabf92d8a294f0000000000ffffffff02f0490200000000001976a914d319d972aefed63bea915210b90495757cd24bae88ac00000000000000002e6a2c54657374466c757853541220ab1f8ca0f3ea808471c3a8dfd9af5562c2c191e53130e41409a879b2ad5e7cf500000000

Then sign with bitcoin-cli signrawtransaction

Signed output:

010000000167fc6d096cbb2c207945240708ab3355f943dbf4995949a4745dabf92d8a294f000000006b48304502210092724d4941aadadfd87383536c56848e75ff6a3c10b81fffcb15325963008841022013c17c960873784dec77f9bc0a02373bbf0b04badb5682145c5a7cb877d8c81401210391d0b7d0eb218cdea2cc3e6c173c58b8ea2eb2a5442849dfc8c519b5658ab419ffffffff02f0490200000000001976a914d319d972aefed63bea915210b90495757cd24bae88ac00000000000000002e6a2c54657374466c757853541220ab1f8ca0f3ea808471c3a8dfd9af5562c2c191e53130e41409a879b2ad5e7cf500000000

Transaction id: c2462ef4e8938fcf05e76c08c40f19f10efc3e9d74da97488fe6b16e8cb4fcc8

# Running Postgres in Docker

/usr/lib/postgresql/9.5/bin/postgres -D /var/lib/postgresql/9.5/main -c config_file=/etc/postgresql/9.5/main/postgresql.conf






## Days worked


Thu 24 Nov
Fri 25 Nov
=========
Mon 28 Nov
Tue 29 Nov
Wed 30 Nov
Thu 01 Dec
Fri 02 Dec
=========
Mon 05 Dec
Tue 06 Dec
  --
Thu 08 Dec
  --
=========
Mon 12 Dec
Tue 13 Dec
Wed 14 Dec
Thu 15 Dec
Fri 16 Dec
==========
Mon 19 Dec
Tue 20 Dec
Wed 21 Dec
Thu 22 Dec
  --
==========
19 days